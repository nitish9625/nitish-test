import { QuestionViewComponent } from './pages/question-view/question-view.component';
import { QuizzeViewComponent } from './pages/quizze-view/quizze-view.component';
import { QuizzeListComponent } from './pages/quizze-list/quizze-list.component';
import { HomeComponent } from './pages/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './authentication/signup/signup.component';
import { LoginComponent } from './authentication/login/login.component';
import { DashboardComponent } from './navbar/dashboard/dashboard.component';

const routes: Routes = [
  {
    path:'', redirectTo:'login', pathMatch:'full'
  },
  {
    path:'login', component:LoginComponent
  },
 {
   path:'register', component:SignupComponent
 },


  {
    path:'dashboard', component:DashboardComponent,
    children:[
      {path:'', component:HomeComponent},
      {path:'quzze-list', component:QuizzeListComponent},
      {path:'quizze-view', component:QuizzeViewComponent}
    ]
  },


  {
    path:'question-view', component:QuestionViewComponent
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
