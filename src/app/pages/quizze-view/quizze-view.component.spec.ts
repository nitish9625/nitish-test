import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuizzeViewComponent } from './quizze-view.component';

describe('QuizzeViewComponent', () => {
  let component: QuizzeViewComponent;
  let fixture: ComponentFixture<QuizzeViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuizzeViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuizzeViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
