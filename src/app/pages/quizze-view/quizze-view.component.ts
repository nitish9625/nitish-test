import { Component, OnInit } from '@angular/core';
import { faQuestionCircle, faFileAlt, faClock } from '@fortawesome/free-solid-svg-icons';


@Component({
  selector: 'app-quizze-view',
  templateUrl: './quizze-view.component.html',
  styleUrls: ['./quizze-view.component.css']
})
export class QuizzeViewComponent implements OnInit {

// font awesome
faQuestion = faQuestionCircle;
faFile = faFileAlt;
faClock = faClock;

  constructor() { }

  ngOnInit(): void {
  }

}
