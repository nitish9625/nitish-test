import { Component, OnInit, Directive, HostListener } from '@angular/core';

@Component({
  selector: 'app-question-view',
  templateUrl: './question-view.component.html',
  styleUrls: ['./question-view.component.css']
})
export class QuestionViewComponent implements OnInit {

  fullscreendisplay: boolean = true

  constructor() { }

  ngOnInit(): void {
  }

  // full screen
elem = document.documentElement;
  fullscreen(){
    if (this.elem.requestFullscreen){
      this.elem.requestFullscreen();
      this.fullscreendisplay=false
    }
  }
  exitscreen(){
    if(document.exitFullscreen){
      document.exitFullscreen();
      this.fullscreendisplay=true
    }
  }

  // event for right click and copy paste
  @HostListener('contextmenu',['$event'])
  onRightClick(event:any){
    event.preventDefault();
  }
  @HostListener('copy', ['$event'])
  blockCopy(e: KeyboardEvent){
    e.preventDefault();
  }
  @HostListener('paste', ['$event']) blockPaste(e: KeyboardEvent) {
    e.preventDefault();
  }
  @HostListener('cut', ['$event']) blockCut(e: KeyboardEvent) {
    e.preventDefault();
  }
  @HostListener('keydown', ['$event'])
  blockKeyDown(e:KeyboardEvent){
    e.stopPropagation();
  }


}


