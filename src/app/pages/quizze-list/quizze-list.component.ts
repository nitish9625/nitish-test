import { Component, OnInit } from '@angular/core';
import { faAddressBook, faUser } from '@fortawesome/free-solid-svg-icons';
import { faUserCircle } from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-quizze-list',
  templateUrl: './quizze-list.component.html',
  styleUrls: ['./quizze-list.component.css']
})
export class QuizzeListComponent implements OnInit {

  // font awesome
  faadd = faAddressBook;
  faUserCircle = faUserCircle;

  constructor() { }

  ngOnInit(): void {
  }

}
